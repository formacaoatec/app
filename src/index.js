import Vue from 'vue';
import axios from 'axios';

new Vue({
    el: "#app",

    data: {
        tasks: []
    },

    methods: {

    },

    mounted() {
        axios.get('http://localhost:8000/api/tasks')
            .then(response => { 
                this.tasks = response.data;
            })
    }
});